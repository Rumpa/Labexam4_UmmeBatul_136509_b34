<!DOCTYPE html>
<html lang="en">
<head>
    <title>Modal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assest/css/bootstrap.min.css">
    <script src="assest/js/jquery.min.js"></script>
    <script src="assest/js/bootstrap.min.js"></script>
    <style>
        .container{
            text-align: center;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Modal</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Click Here</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enter your data</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="task2.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="picture">Picture:</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" id="picture" name="image">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="description">Description:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="description" name="description">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="date">Date:</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="date" name="date">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Send</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

</div>

</body>
</html>

