<!DOCTYPE html>
<html lang="en">
<head>
    <title>Carousel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assest/css/bootstrap.min.css">
    <script src="assest/js/jquery.min.js"></script>
    <script src="assest/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>
</head>
<body>

<div class="container">
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <?php $image_name=time().$_FILES['image']['name'];
                      $temporary_location=$_FILES['image']['tmp_name'];
                      $mylocation='image/'.$image_name;
                      move_uploaded_file($temporary_location,$mylocation);
                ?>
                <img src="<?php echo $mylocation; ?>" alt="picture" width="460" height="345">
                <div class="carousel-caption">
                    <h3><?php echo $_POST['description']; ?></h3>
                    <p><?php echo $_POST['date']; ?></p>
                </div>
            </div>

            <div class="item">
                <img src="<?php echo $mylocation; ?>" alt="picture" width="460" height="345">
                <div class="carousel-caption">
                    <h3><?php echo $_POST['description']; ?></h3>
                    <p><?php echo $_POST['date']; ?></p>
                </div>
            </div>

            <div class="item">
                <img src="<?php echo $mylocation; ?>" alt="picture" width="460" height="345">
                <div class="carousel-caption">
                    <h3><?php echo $_POST['description']; ?></h3>
                    <p><?php echo $_POST['date']; ?></p>
                </div>
            </div>



        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

</body>
</html>

